RG=azdev
echo "Creating Azure Resource Group"
az group create --location eastus -n ${RG}

echo "Creating Azure Virtual Network"
az network vnet create -g ${RG} -n ${RG}-vNET1 --address-prefix 10.1.0.0/16 \
--subnet-name ${RG}-Subnet-1 --subnet-prefix 10.1.1.0/24 -l eastus

az vm create --resource-group ${RG} --name PRODAGENT --image UbuntuLTS --vnet-name ${RG}-vNet1 \
--subnet ${RG}-Subnet-1 --admin-username saivishnu --admin-password "Quards1!" --size Standard_B2ms \
--nsg ""

az vm create --resource-group ${RG} --name DEVAGENT --image UbuntuLTS --vnet-name ${RG}-vNet1 \
--subnet ${RG}-Subnet-1 --admin-username saivishnu --admin-password "Quards1!" --size Standard_B2s \
--nsg ""

watch ls

